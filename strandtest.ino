#include <Adafruit_NeoPixel.h>
#include <Wire.h>
#include <Time.h>  
#include <DS1307.h>
#define PIN 6
// multiprupose counter
int counter = 0;
// last second
uint8_t lastsecond = 0;
// clock option fade seconds
boolean coptionfade = 1;

// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(60, PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  Serial.begin(9600);
  setSyncProvider(RTC.get);
  setSyncInterval(1);
  strip.begin();
  strip.show();
  lasttime = millis();
  currenttime = millis();
  lastsecond = second();
  color_ambient = strip.Color(0, 180, 255);
  //pinMode(A0, INPUT);
  //pinMode(BUTTON_PIN, INPUT);
  //digitalWrite(BUTTON_PIN, HIGH);
  delay(20);
  //strip.begin();
  //strip.show(); // Initialize all pixels to 'off'
}

void loop() {
  // Some example procedures showing how to display to the pixels:
  colorWipe(strip.Color(255, 0, 0), 50); // Red
  colorWipe(strip.Color(0, 255, 0), 50); // Green
  colorWipe(strip.Color(0, 0, 255), 50); // Blue
  rainbow(20);
  rainbowCycle(20);
}

// clock mode
void clockMode() {
  time_t t = now();
  uint8_t analoghour = hour(t);
  uint8_t currentsecond = second(t);

  if (analoghour > 12) {
    analoghour=(analoghour-12);
  }
  analoghour = analoghour*5+(minute(t)/12);

  lightPixels(strip.Color(2, 2, 2));

  //  if (coptionfivemin) {
  //    for (uint8_t i=0; i<60; i += 5) {
  //      strip.setPixelColor(i,strip.Color(10, 10, 10));
  //    }
  //  }

  strip.setPixelColor(pixelCheck(analoghour-1),strip.Color(70, 0, 0));
  strip.setPixelColor(pixelCheck(analoghour),strip.Color(255, 0, 0));
  strip.setPixelColor(pixelCheck(analoghour+1),strip.Color(70, 0, 0));

  strip.setPixelColor(minute(t),strip.Color(0, 0, 255));

  if (coptionfade) {
    // reset counter
    if(counter>25) {
      counter = 0;
    }
    else if (lastsecond != currentsecond) {
      lastsecond = second();
      counter = 0;  
    }
    strip.setPixelColor(pixelCheck(second(t)+1),strip.Color(0, counter*10, 0));  
    strip.setPixelColor(second(t),strip.Color(0, 255-(counter*10), 0));
    counter++;
  }
  else {
    strip.setPixelColor(second(t),strip.Color(0, 255, 0));
  }
} 


// light all pixels with given values
void lightPixels(uint32_t c) {
  for (uint8_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i,c);
  }
}

// set the correct pixels
int pixelCheck(int i) {
  if (i>59) {
    i = i - 60;
  }
  if (i<0) {
    i = i +60;
  }
  return i;
}


// Fill the dots one after the other with a color
void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, c);
    strip.show();
    delay(wait);
  }
}

void rainbow(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256; j++) {
    for(i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel((i+j) & 255));
    }
    strip.show();
    delay(wait);
  }
}

// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel
    for(i=0; i< strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels()) + j) & 255));
    }
    strip.show();
    delay(wait);
  }
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  if(WheelPos < 85) {
    return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } 
  else if(WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } 
  else {
    WheelPos -= 170;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}


