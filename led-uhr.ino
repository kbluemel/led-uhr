#
##include <Adafruit_NeoPixel.h>
##include <Wire.h>
##include <Time.h>  
##include <DS1307RTC.h> 
#
#//Definitionen
##define LED_PIN 6 // LED strip pin
#
#Adafruit_NeoPixel strip = Adafruit_NeoPixel(60, LED_PIN, NEO_GRB + NEO_KHZ800); 
#
#// initialize everything
#void setup() {
#  Serial.begin(9600);
#  setSyncProvider(RTC.get);
#  setSyncInterval(1);
#  strip.begin();
#  strip.show();
#  lasttime = millis();
#  currenttime = millis();
#  lastsecond = second();
#  color_ambient = strip.Color(0, 180, 255);
#  strip.begin();
#  strip.show();
#  delay(20);
#} 
#
#
#// Hauptschleife
#void(loop(){
#	clockMode();
#}
#
#// clock mode
#void clockMode() {
#  time_t t = now();
#  uint8_t analoghour = hour(t);
#  uint8_t currentsecond = second(t);
#
#  if (analoghour > 12) {
#    analoghour=(analoghour-12);
#  }
#  analoghour = analoghour*5+(minute(t)/12);
#
#  lightPixels(strip.Color(2, 2, 2));
#
#  //  if (coptionfivemin) {
#  //    for (uint8_t i=0; i<60; i += 5) {
#  //      strip.setPixelColor(i,strip.Color(10, 10, 10));
#  //    }
#  //  }
#
#  strip.setPixelColor(pixelCheck(analoghour-1),strip.Color(70, 0, 0));
#  strip.setPixelColor(pixelCheck(analoghour),strip.Color(255, 0, 0));
#  strip.setPixelColor(pixelCheck(analoghour+1),strip.Color(70, 0, 0));
#
#  strip.setPixelColor(minute(t),strip.Color(0, 0, 255));
#
#  if (coptionfade) {
#    // reset counter
#    if(counter>25) {
#      counter = 0;
#    }
#    else if (lastsecond != currentsecond) {
#      lastsecond = second();
#      counter = 0;  
#    }
#    strip.setPixelColor(pixelCheck(second(t)+1),strip.Color(0, counter*10, 0));  
#    strip.setPixelColor(second(t),strip.Color(0, 255-(counter*10), 0));
#    counter++;
#  }
#  else {
#    strip.setPixelColor(second(t),strip.Color(0, 255, 0));
#  }
#} 
#
#// light all pixels with given values
#void lightPixels(uint32_t c) {
#  for (uint8_t i=0; i<strip.numPixels(); i++) {
#    strip.setPixelColor(i,c);
#  }
#}
#
#// set the correct pixels
#int pixelCheck(int i) {
#  if (i>59) {
#    i = i - 60;
#  }
#  if (i<0) {
#    i = i +60;
#  }
#  return i;
#}
#
#//Uhr
#
#void uhr(){
#	allepixel(2,2,2);
#    for (uint8_t i=0; i<strip.numPixels(); i++) {
#      strip.setPixelColor(i,strip.Color(10, 10, 10));
#	  delay(1000);
#	  }
#}
#
#// Alle Pixel mit vorgegebener Farbe einschalten 
#void allepixel(uint32_t c) {
#  for (uint8_t i=0; i<strip.numPixels(); i++) {
#    strip.setPixelColor(i,c);
#  }
#} 